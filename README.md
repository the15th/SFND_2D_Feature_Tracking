# SFND 2D Feature Tracking

<img src="images/keypoints.png" width="820" height="248" />

Feature tracking part and testing various detector / descriptor combinations to see which ones perform best. 

## Data Buffer
Implemented a vector for dataBuffer objects whose size does not exceed a limit (e.g. 2 elements) using a std::deque

## Keypoints
Implemented detectors HARRIS, FAST, BRISK, ORB, AKAZE, and SIFT selectable by setting a string accordingly.  
Removed all keypoints outside of a pre-defined rectangle and only used the keypoints within the rectangle for further processing.

## Descriptors
Implemented descriptors BRIEF, ORB, FREAK, AKAZE and SIFT and make them selectable by setting a string accordingly.  
Implemented FLANN matching and k-nearest neighbor selection.  
Used the K-Nearest-Neighbor matching to implement the descriptor distance ratio test, which looks at the ratio of best vs. second-best match to decide whether to keep an associated pair of keypoints.

## Performance
All tests was performed on a CPU AMD Ryzen 5 3550H

## Keypoints performance evaluation
|Detector   | Avg Keypoints on the 10 input images  |
|---|---|
| SHITOMASI | 1342 | 
|HARRIS | 174 |  
| FAST | 2292 |
| BRISK |	2712 |
| ORB | 500 |
| AKAZE | 1343 |
| SIFT | 1386 |

## Matches performance evaluation
|Detector-Descriptor   | BRIEF   | ORB | FREAK |SIFT| AKAZE|
|---|---|---|---|---|---|
|SHITOMASI| 1231| 1220 | 1247 | 1343|
|HARRIS| 154 | 152 | 155 | 162|
|FAST| 1918 | 1880 | 1978 | 2276|
|BRISK| 2687 | 2674 | 2302 | 2716|
|ORB| 500 | 500 | 164 | 500|
|AKAZE|
|SIFT| 1318 | | 1256 | 1382

## Average Processing Time (ms) on all input images for each detector + descriptor combination
|Detector-Descriptor   | BRIEF   | ORB | FREAK |SIFT| AKAZE|
|---|---|---|---|---|---|
|SHITOMASI| 21.14 | 20.182 | 19.43 | 20.7 |
|HARRIS| 22.45 | 16.01 | 17.85 | 16.34 |
|FAST| 6.98 | 6.12 | 6.87 | 6.82 |
|BRISK| 192.3 | 188.6 | 186.3 | 191.18 |
|ORB| 9.85 | 8.45 | 8.22 |  7.99|
|AKAZE| 80.85 | 80.87 | 86.47 | 80.91 | 79.47 |
|SIFT| 107.144 | ND | 104.40 | 104.90|

## Top three combinations
FAST-FREAK, FAST-OR, FAST-SIFT.

## Dependencies for Running Locally
* cmake >= 2.8
  * All OSes: [click here for installation instructions](https://cmake.org/install/)
* make >= 4.1 (Linux, Mac), 3.81 (Windows)
  * Linux: make is installed by default on most Linux distros
  * Mac: [install Xcode command line tools to get make](https://developer.apple.com/xcode/features/)
  * Windows: [Click here for installation instructions](http://gnuwin32.sourceforge.net/packages/make.htm)
* OpenCV >= 4.1
  * This must be compiled from source using the `-D OPENCV_ENABLE_NONFREE=ON` cmake flag for testing the SIFT and SURF detectors.
  * The OpenCV 4.1.0 source code can be found [here](https://github.com/opencv/opencv/tree/4.1.0)
* gcc/g++ >= 5.4
  * Linux: gcc / g++ is installed by default on most Linux distros
  * Mac: same deal as make - [install Xcode command line tools](https://developer.apple.com/xcode/features/)
  * Windows: recommend using [MinGW](http://www.mingw.org/)

## Basic Build Instructions

1. Clone this repo.
2. Make a build directory in the top level directory: `mkdir build && cd build`
3. Compile: `cmake .. && make`
4. Run it: `./2D_feature_tracking`.