/* INCLUDES FOR THIS PROJECT */
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <limits>
#include <deque>
#include <map>
#include <opencv2/core.hpp>
#include <opencv2/core/utils/filesystem.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>
#include "dataStructures.h"
#include "matching2D.hpp"
#include <numeric>
#include <chrono>


void computeFeatureTracking(const std::string& detectorType,
    const std::string& descriptorType,
    const std::string& matcherType,
    const std::string& descriptorCategory,
    const std::string& selectorType,
    std::vector<int>& nrKeypointsOut,
    std::vector<int>& nrMatchesOut,
    std::vector<double>& nrTimeProcessed) {

    /* INIT VARIABLES AND DATA STRUCTURES */
    // data location
    std::string dataPath = "../";

    // camera
    std::string imgBasePath = dataPath + "images/";
    std::string imgPrefix = "KITTI/2011_09_26/image_00/data/000000"; // left camera, color
    std::string imgFileType = ".png";
    int imgStartIndex = 0; // first file index to load (assumes Lidar and camera names have identical naming convention)
    int imgEndIndex = 9;   // last file index to load
    int imgFillWidth = 4;  // no. of digits which make up the file index (e.g. img-0001.png)

    // misc
    int dataBufferSize = 2;       // no. of images which are held in memory (ring buffer) at the same time
    std::deque<DataFrame> dataBuffer; // list of data frames which are held in memory at the same time
    bool bVis = false;            // visualize results

    /* MAIN LOOP OVER ALL IMAGES */

    for (size_t imgIndex = 0; imgIndex <= imgEndIndex - imgStartIndex; imgIndex++)
    {
        /* LOAD IMAGE INTO BUFFER */
        std::cout << "|||||||||||||||||||||||||||||||" << std::endl;
        std::cout << "Image " << imgIndex + 1 << std::endl;

        // assemble filenames for current index
        std::ostringstream imgNumber;
        imgNumber << std::setfill('0') << std::setw(imgFillWidth) << imgStartIndex + imgIndex;
        std::string imgFullFilename = imgBasePath + imgPrefix + imgNumber.str() + imgFileType;

        // load image from file and convert to grayscale
        cv::Mat img, imgGray;
        if (!cv::utils::fs::exists(imgFullFilename)) {
            std::cerr << " wrong dataPath... " << std::endl;
            exit(1);
        }

        img = cv::imread(imgFullFilename);
        cv::cvtColor(img, imgGray, cv::COLOR_BGR2GRAY);

        //// STUDENT ASSIGNMENT
        //// TASK MP.1 -> replace the following code with ring buffer of size dataBufferSize

        // push image into data frame buffer
        DataFrame frame;
        frame.cameraImg = imgGray;
        dataBuffer.push_back(frame);
        if (dataBuffer.size() > dataBufferSize)
            dataBuffer.pop_front();

        //// EOF STUDENT ASSIGNMENT
        std::cout << "#1 : LOAD IMAGE INTO BUFFER done" << std::endl;

        /* DETECT IMAGE KEYPOINTS */

        // extract 2D keypoints from current image
        std::vector<cv::KeyPoint> keypoints; // create empty feature list for current image
        std::map<std::string, std::vector<cv::KeyPoint>> keypointsResults;


        std::cout << "-----------------------------------------------" << std::endl;
        std::cout  << detectorType << " detector " << std::endl;

        //// STUDENT ASSIGNMENT
        //// TASK MP.2 -> add the following keypoint detectors in file matching2D.cpp and enable string-based selection based on detectorType
        //// -> HARRIS, FAST, BRISK, ORB, AKAZE, SIFT

        double timeProcessed = 0;
        auto start = std::chrono::steady_clock::now();
        if (detectorType.compare("SHITOMASI") == 0)
        {
            detKeypointsShiTomasi(keypoints, imgGray, false);
        }
        else if (detectorType.compare("HARRIS") == 0)
        {
            detKeypointsHarris(keypoints, imgGray, false);
        }
        else
        {
            detKeypointsModern(keypoints, img, detectorType, false);
        }
        auto end = std::chrono::steady_clock::now();
        timeProcessed = std::chrono::duration<double, std::milli>(end - start).count();
        nrKeypointsOut.push_back(keypoints.size());

        //// EOF STUDENT ASSIGNMENT

        //// STUDENT ASSIGNMENT
        //// TASK MP.3 -> only keep keypoints on the preceding vehicle
        // only keep keypoints on the preceding vehicle
        bool bFocusOnVehicle = true;
        cv::Rect vehicleRect(535, 180, 180, 150);
        if (bFocusOnVehicle)
        {
            keypoints.erase(std::remove_if(keypoints.begin(), keypoints.end(), [&vehicleRect](const cv::KeyPoint& kp)
                {
                    return !vehicleRect.contains(kp.pt);
                }), keypoints.end());
        }

        //// EOF STUDENT ASSIGNMENT
        // push keypoints and descriptor for current frame to end of data buffer
        (dataBuffer.end() - 1)->keypoints = keypoints;
        std::cout << "#2 : DETECT KEYPOINTS done" << std::endl;
        std::cout << "-----------------------------------------------" << std::endl;

        /* EXTRACT KEYPOINT DESCRIPTORS */

        //// STUDENT ASSIGNMENT
        //// TASK MP.4 -> add the following descriptors in file matching2D.cpp and enable string-based selection based on descriptorType
        //// -> BRIEF, ORB, FREAK, AKAZE, SIFT
        cv::Mat descriptors;

        std::cout << "-----------------------------------------------" << std::endl;
        std::cout << "Using descriptor " << descriptorType << std::endl;
        descKeypoints((dataBuffer.end() - 1)->keypoints, (dataBuffer.end() - 1)->cameraImg, descriptors, descriptorType);
        //// EOF STUDENT ASSIGNMENT

        // push descriptors for current frame to end of data buffer
        (dataBuffer.end() - 1)->descriptors = descriptors;

        std::cout << "#3 : EXTRACT DESCRIPTORS done" << std::endl;
        std::cout << "-----------------------------------------------" << std::endl;
        if (dataBuffer.size() > 1) // wait until at least two images have been processed
        {

            /* MATCH KEYPOINT DESCRIPTORS */

            std::vector<cv::DMatch> matches;
            //std::string matcherType = "MAT_BF";        // MAT_BF, MAT_FLANN
            //std::string descriptorCategory = "DES_BINARY"; // DES_BINARY, DES_HOG
            //std::string selectorType = "SEL_NN";       // SEL_NN, SEL_KNN

            //// STUDENT ASSIGNMENT
            //// TASK MP.5 -> add FLANN matching in file matching2D.cpp
            //// TASK MP.6 -> add KNN match selection and perform descriptor distance ratio filtering with t=0.8 in file matching2D.cpp

            start = std::chrono::steady_clock::now();
            try {
                matchDescriptors((dataBuffer.end() - 2)->keypoints, (dataBuffer.end() - 1)->keypoints,
                    (dataBuffer.end() - 2)->descriptors, (dataBuffer.end() - 1)->descriptors,
                    matches, descriptorCategory, matcherType, selectorType);
            }
            catch (std::exception& e) {
                std::cout << e.what() << std::endl;
                std::cout << "matchDescriptors, error with " << detectorType << " " << descriptorType << " " << descriptorCategory << " " << matcherType << " " << selectorType << std::endl;
                std::cout << std::endl;
            }
            end = std::chrono::steady_clock::now();
            timeProcessed += std::chrono::duration<double, std::milli>(end - start).count();
            nrMatchesOut.push_back(matches.size());
            nrTimeProcessed.push_back(timeProcessed);

            //// EOF STUDENT ASSIGNMENT

            // store matches in current data frame
            (dataBuffer.end() - 1)->kptMatches = matches;

            std::cout << "#4 : MATCH KEYPOINT DESCRIPTORS done" << std::endl;
            std::cout << "-----------------------------------------------" << std::endl;

            // visualize matches between current and previous image
            bVis = false;
            if (bVis)
            {
                cv::Mat matchImg = ((dataBuffer.end() - 1)->cameraImg).clone();
                cv::drawMatches((dataBuffer.end() - 2)->cameraImg, (dataBuffer.end() - 2)->keypoints,
                    (dataBuffer.end() - 1)->cameraImg, (dataBuffer.end() - 1)->keypoints,
                    matches, matchImg,
                    cv::Scalar::all(-1), cv::Scalar::all(-1),
                    std::vector<char>(), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);

                std::string windowName = "Matching keypoints between two camera images";
                cv::namedWindow(windowName, 7);
                cv::imshow(windowName, matchImg);
                std::cout << "Press key to continue to next image" << std::endl;
                cv::waitKey(0); // wait for key to be pressed
            }
            bVis = false;

        }
    } // eof loop over all images
}

/* MAIN PROGRAM */
int main(int argc, const char* argv[])
{

    std::vector<std::string> detectorTypeList = { "SHITOMASI",  "HARRIS", "FAST", "BRISK", "ORB", "AKAZE", "SIFT" };
    std::vector<std::string> descriptorTypeList = { "BRIEF", "ORB", "FREAK", "AKAZE", "SIFT" };
    

    std::ofstream keypointsStream("keypoints_benchmark.txt");
    std::ofstream matchesStream("matches_benchmark.txt");
    std::ofstream timingStream("timing_benchmark.txt");


    for (auto detectorType : detectorTypeList) {

        for (auto descriptorType : descriptorTypeList) {
            std::vector<int> nrKeypointsOut;
            std::vector<int> nrMatchesOut;
            std::vector<double> nrTimeProcessed;

            if (detectorType != "AKAZE" && descriptorType == "AKAZE")
                continue;
            if (detectorType == "SIFT" && descriptorType == "ORB")
                continue;
            if (descriptorType == "SIFT")
                computeFeatureTracking(detectorType,
                    descriptorType,
                    "MAT_BF",
                    "DES_HOG",
                    "SEL_NN",
                    nrKeypointsOut, nrMatchesOut, nrTimeProcessed);
            else
                computeFeatureTracking(detectorType,
                    descriptorType,
                    "MAT_BF",
                    "DES_BINARY",
                    "SEL_NN",
                    nrKeypointsOut, nrMatchesOut, nrTimeProcessed);
                
            auto meanKeypoints = std::accumulate(std::begin(nrKeypointsOut), std::end(nrKeypointsOut), 0.0) / nrKeypointsOut.size();
            auto meanMatches = std::accumulate(std::begin(nrMatchesOut), std::end(nrMatchesOut), 0.0) / nrMatchesOut.size();
            auto meanTime = std::accumulate(std::begin(nrTimeProcessed), std::end(nrTimeProcessed), 0.0) / nrTimeProcessed.size();

            keypointsStream << detectorType << "  " << descriptorType << " " << std::endl;
            keypointsStream << " avg keypoints " << meanKeypoints << std::endl;

            matchesStream << detectorType << "  " << descriptorType << " " << std::endl;
            matchesStream << " avg matches " << meanMatches << std::endl;

            timingStream << detectorType << "  " << descriptorType << " " << std::endl;
            timingStream << "avg time " << meanTime << std::endl;
        }   
    }
    keypointsStream.close();
    matchesStream.close();
    return 0;
}
